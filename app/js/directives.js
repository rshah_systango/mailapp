'use strict';
/* Directives */

var mailAppDirectives = angular.module('mailAppDirectives', []);

mailAppDirectives.directive('widgetGenerator',function(){
  return{
  restrict:'E',
  templateUrl: 'partials/widgetview.html',
  controller:'MailWidgetCtrl'
  };
});

mailAppDirectives.directive('mailGenerator',function(){
  return{
  restrict:'E',
  templateUrl: 'partials/mail-view.html',
  controller: 'MailViewController'
  };
});