'use strict';

/* Controllers */

var mailAppControllers = angular.module('mailAppControllers', []);

mailAppControllers.controller('MailCtrl', ['$routeParams','$scope','Mail',function($routeParams,$scope,Mail){
 	$scope.LoadFile = function(fname) {
    
 		$routeParams.filename=fname;
  	return Mail.query({filename:$routeParams.filename});
	};
}
]);

mailAppControllers.controller('MailWidgetCtrl', ['$routeParams','$scope','Mail',function($routeParams,$scope,Mail){
   $scope.length=5; 
   $scope.count = 0 ;  
  // $scope.Filename = $routeParams.filename;

 	 //$scope.Mails='';
  $scope.widgetLength=function(){
    return $scope.length;
  };
  $scope.dynamicLoadFile = function(fname) {
 		$routeParams.filename=fname;
  	$scope.Mails = Mail.query({filename:$routeParams.filename});
     
  };

	$scope.showText = function(text) {
 		alert(text);
	};

  $scope.increment = function(){
   $scope.count =$scope.count+1;
   return $scope.count  
  };

  $scope.showTextForBox = function(count){
     if($scope.Mails.length >0 && $scope.Mails[count].text!=undefined){
    return $scope.Mails[count].text;
    }
  };

  $scope.showNumberForBox = function(count){
       
      if($scope.Mails.length >0 && $scope.Mails[count].text!=undefined){
        return $scope.Mails[count].number;
        }
  };

  $scope.showIdForBox = function(count){
     if($scope.Mails.length >0 && $scope.Mails[count].text!=undefined){
        return $scope.Mails[count].id;}
  };
  
  $scope.getNumber = function(num) {
    return new Array(num);   
  };  

  $scope.applyClass = function(num) {
    num=num+1;
    var box_class="box"+num;
    return box_class;
  };

  $scope.getCount = function(num) {
    num=num+1;
    var box_count="count"+num;
    return box_count;
  };
}
]);

mailAppControllers.controller('MailViewController',['$routeParams','$scope','Mail',function($routeParams,$scope){
$scope.length=5; 
$scope.getPanes = function(num) {
    return new Array(num);   
  };  

}

]);
