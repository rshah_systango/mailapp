'use strict';
/* Services */
var mailAppServices= angular.module('mailAppServices',['ngResource']);

mailAppServices.factory('Mail',['$resource',
	function($resource){
		return $resource('mails/:filename', {}, {
			query: {method:'GET',params:{filename:'filename'}, isArray:true}
		});
	}]);