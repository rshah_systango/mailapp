'use strict';
var mailApp = angular.module('mailApp', [
  'ngRoute',
  
  'mailAppControllers',
  'mailAppServices'
]);
mailApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
