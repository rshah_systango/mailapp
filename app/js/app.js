'use strict';

var mailApp= angular.module('mailApp',[ 
'mailAppControllers',
'mailAppServices',
'ngRoute'

]);

mailApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
    when('/mails/:filename', {
        templateUrl:'partials/mail-view.html',
        controller: 'MailDetailCtrl'
      });
}]);
